
import java.util.Scanner;

public class Programa {
		
	public static void main(String[] args) {
			
		Scanner leitor = new Scanner(System.in);
		
		//String nome = "Ana";
		String nome;
		
		System.out.println("Digite um nome:");
		nome = leitor.nextLine();
		
		System.out.println(nome);
		
		
		int numeroQualquer;
		System.out.println("Digite um número inteiro:");
		numeroQualquer = leitor.nextInt();
				
		
		double numeroDecimal;
		System.out.println("Digite um número decimal:");
		numeroDecimal = leitor.nextDouble();
				
		if (numeroQualquer >= 100) {
			System.out.println(numeroQualquer);	
		}
		
		if (numeroDecimal >= 100) {
			System.out.println(numeroDecimal);	
		} 
		
		
		
		
	}

}
